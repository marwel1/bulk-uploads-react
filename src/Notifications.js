import React, { useState, useEffect } from 'react'
import request from 'superagent'
import './notif.css'
const Notifications = () => {

    const [state, setstate] = useState()

    const testRun = () => { 
        const reqNotif = request
            // .get('https://cors-anywhere.herokuapp.com/https://test-api.service.hmrc.gov.uk/customs/goods-movement-system/movements')
            .get('http://localhost:8080/push')
            .accept('application/vnd.hmrc.1.0+json')
            .set('Authorization', `Bearer 9d611a776b11ff64f9a6788637742756`)
            .then((resp) => setstate(resp.body))
            .catch((err) => console.log(err))
    }

    useEffect(() => {
        testRun()
    }, [])
    console.log('state', state && JSON.parse(state[1].message)) // JSON.parse(state[0].message)

    return (
        <div className='main'>
            <h3>BASIC NOTIFICATIONS EXAMPLES</h3>
            {
                state && state.map(res => 
                    <div className='container' key={JSON.parse(res.message).gmrId}>
                        <p>GMR Id: <br/><span>{JSON.parse(res.message).gmrId}</span></p>
                        <p>GMR state: <br/><span>{JSON.parse(res.message).gmrState}</span></p>
                        <p>Date {'&'} time Created: <br/><span>{new Date(JSON.parse(res.message).createdDateTime).toLocaleString()}</span></p>
                        <p>Date {'&'} time of last update: <br/><span>{new Date(JSON.parse(res.message).updatedDateTime).toLocaleString()}</span></p>
                        {
                            JSON.parse(res.message).ruleFailures &&
                            <div>
                                <p><br/>Rule Failures</p>
                            {
                                JSON.parse(res.message).ruleFailures.map( rf => 
                                    <p key={rf.technicalMessage}>• <span>{rf.technicalMessage}</span></p>
                                )
                            }
                            </div>
                        }
                    </div>
                )
            }
        </div>
    )
}

export default Notifications
