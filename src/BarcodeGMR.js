import React, {createRef} from 'react';
import Barcode from 'react-barcode'
import Pdf from "react-to-pdf"
import './barcode.css'

const BarcodeGMR = () => {
    const ref = createRef()

    const placeholder = {
        "gmrId": "GMRO00000A9C",
        "direction": "GB_TO_NI",
        "vehicleRegNum": "GU51 ABC",
        "state": "OPEN",
        "plannedCrossing": {
        "routeId": "61",
        "localDateTimeOfDeparture": "2021-08-11T10:58"
        }
    }

    return (
        <div className='main' ref={ref}>
            <div className='inner1'>
                <h2>Present this GMR at the port</h2>
                <Barcode value={placeholder.gmrId} height='120' width='3' fontSize='30'/>
            </div>

            <div className='inner2'>
                <p>Planned Route: <br/><span>{placeholder.direction}</span></p>
                <p>Vehicle Registration Numbers: <br/><span>{placeholder.vehicleRegNum}</span></p>
                <p>Trailer Numbers: <br/><span>KF293716, GH28372S</span></p>
            </div>

            <Pdf targetRef={ref} filename={`${placeholder.gmrId}-Barcode.pdf`}>
                {({ toPdf }) => <button onClick={toPdf}>Download as PDF</button>}
            </Pdf>
        </div>
    )
}

export default BarcodeGMR
