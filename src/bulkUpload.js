import React, { useEffect } from 'react'
import { pickBy, identity } from 'lodash'
import axios from 'axios'
import request from 'superagent'

const BulkUpload = ({bulkData}) => { 

    // GET GMR(s) from excel, cleans data to conform to GVMS's Schemas and send multiple request according to the length of array to the API
    const pushData = async () => {
        const result = Object.values(bulkData.filter(resp => resp.Direction && resp.Direction.length !== 0 ))
        const DataContainer = []
        const toFilter = []
        
        result.map(res => DataContainer.push({
            direction: 
                res['Direction'] ? res['Direction'] : undefined,
            isUnaccompanied: 
                res['Unaccompanied'] ? JSON.parse(res['Unaccompanied'].toLowerCase()) : undefined,
            vehicleRegNum: 
                res['Vehicle Registration Number'] ? res['Vehicle Registration Number'] : undefined,
            trailerRegistrationNums: 
                res['Trailer Registration Numbers'] ? res['Trailer Registration Numbers'].split(";") : undefined,
            containerReferenceNums: 
                res['Container Reference Numbers'] ?  res['Container Reference Numbers'].split(";") : undefined,
            sAndSMasterRefNum: 
                res['Master Reference Number']? res['Master Reference Number'] : undefined,

            PC_routeId: 
                res['Route ID'] ? res['Route ID'] : undefined,
            PC_localDateTimeOfDeparture: 
                res['(PC)Local Date & Time of Departure'] ? res['(PC)Local Date & Time of Departure'] : undefined,
        
            CD_customsDeclarationId: 
                res['Customs Declarations ID'] ? res['Customs Declarations ID'].split(";") : undefined,
            CD_sAndSMasterRefNum: 
                res['CD-Master Reference Number'] ? res['CD-Master Reference Number'].split(";") : undefined,
            
            TD_transitDeclarationId: 
                res['Transit Declaration ID'] ? res['Transit Declaration ID'].split(";") : undefined,     
            TD_isTSAD: 
                res['TSAD'] ? res['TSAD'].split(";") : undefined,
            TD_sAndSMasterRefNum: 
                res['TD-Master Reference Number'] ? res['TD-Master Reference Number'].split(";") : undefined,
         
            EIDR_traderEORI: 
                res['Trader EORI'] ? res['Trader EORI'].split(";") : undefined,     
            EIDR_sAndSMasterRefNum: 
                res['EIDR-Master Reference Number'] ? res['EIDR-Master Reference Number'].split(";")  : undefined,
          
            EV_isOwnVehicle: 
                res['Own Vehicle'] ? res['Own Vehicle'] : undefined,
            EV_sAndSMasterRefNum: 
                res['Empty-Master Reference Number'] ? res['Empty-Master Reference Number']: undefined,
       
            ATA_ataCarnetId: 
                res['ATA Carnet ID'] ? res['ATA Carnet ID'].split(";") : undefined,
          
            TIR_tirCarnetId: 
                res['TIR Carnet ID'] ? res['TIR Carnet ID'].split(";") : undefined,
           
        }))

        for(let c = 0; c < DataContainer.length; c++ ) {

            toFilter.push(pickBy({
                direction: DataContainer[c].direction,
                isUnaccompanied: DataContainer[c].isUnaccompanied,
                vehicleRegNum: DataContainer[c].vehicleRegNum,
                trailerRegistrationNums: DataContainer[c].trailerRegistrationNums,
                containerReferenceNums: DataContainer[c].containerReferenceNums,
                sAndSMasterRefNum: DataContainer[c].sAndSMasterRefNum,
                plannedCrossing: {
                    routeId: DataContainer[c].PC_routeId,
                    localDateTimeOfDeparture: DataContainer[c].PC_localDateTimeOfDeparture
                },
                customsDeclarations: 
                    DataContainer[c].CD_customsDeclarationId?
                    (
                        DataContainer[c].CD_customsDeclarationId.map((cd, index) => {
                           const mrn = DataContainer[c].CD_sAndSMasterRefNum[index]
                           return {
                            customsDeclarations: cd,
                            sAndSMasterRefNum: mrn
                           }
                        })
                    ): undefined,
                transitDeclarations: 
                    DataContainer[c].TD_transitDeclarationId?
                    (
                        DataContainer[c].TD_transitDeclarationId.map((cd, index) => {
                            const mrn = DataContainer[c].TD_sAndSMasterRefNum[index]
                            const tsad = DataContainer[c].TD_isTSAD[index]
                            return {
                                transitDeclarationId: cd,
                                isTSAD: tsad,
                                sAndSMasterRefNum : mrn
                            }
                        })
                    ): undefined,
                eidrDeclarations: 
                    DataContainer[c].EIDR_traderEORI?
                    (
                        DataContainer[c].EIDR_traderEORI.map((cd, index) => {
                            const mrn = DataContainer[c].EIDR_sAndSMasterRefNum[index]
                            return {
                                traderEORI: cd,
                                sAndSMasterRefNum : mrn
                            }
                        })
                    ): undefined,
                emptyVehicle: 
                    DataContainer[c].EV_isOwnVehicle ?
                    {
                        isOwnVehicle: DataContainer[c].EV_isOwnVehicle,
                        sAndSMasterRefNum: DataContainer[c].EV_sAndSMasterRefNum
                    }: undefined,
                ataDeclarations: 
                    DataContainer[c].ATA_ataCarnetId ?
                    (
                        DataContainer[c].ATA_ataCarnetId.map(id => {
                            return {ataCarnetId: id}
                        })
                    ): undefined,
                tirDeclarations: 
                    DataContainer[c].TIR_tirCarnetId ?
                    (
                        DataContainer[c].TIR_tirCarnetId.map(id => {
                            return {tirCarnetId: id}
                        })
                    ): undefined,
            }, identity))
        }

        // Bulk uploads by multiple http request
        for(let counter = 0; counter < toFilter.length; counter++) {
            const reqNotif = request
                    .post('https://cors-anywhere.herokuapp.com/https://test-api.service.hmrc.gov.uk/customs/goods-movement-system/movements')
                    // .post('https://test-api.service.hmrc.gov.uk/customs/goods-movement-system/movements')
                    .send(toFilter[1])
                    .accept('application/vnd.hmrc.1.0+json')
                    .set('Authorization', `Bearer 9d611a776b11ff64f9a6788637742756`)
                    .then((resp) => console.log(resp))
                    .catch((err) => console.log(err))

            console.log(reqNotif.data)
        }
    } 

    useEffect(() => {}, [])

    return (
        <div>
            <button onClick={pushData}>Upload</button>
        </div>
    )
}

export default BulkUpload
