import {Component} from 'react'
import XLSX from 'xlsx'
import BulkUpload from './bulkUpload'

const make_cols = refstr => {
    let o = [], C = XLSX.utils.decode_range(refstr).e.c + 1
    for(let i = 0; i < C; ++i) o[i] = {name:XLSX.utils.encode_row(i), key:i} 
    return o
}

const SheetJSFT = [
    "xlsx", "xlsb", "xlsm", "xls", "xml", "csv", "txt", "ods", "fods", "uos", "sylk", "dif", "dbf", "prn", "qpw", "123", "wb*", "wq*", "html", "htm"
].map(function(x) { return "." + x }).join(",")

class ExcelReader extends Component {
    constructor(props) {
      super(props)
      this.state = {
        file: {},
        data: [],
        cols: [],
        display: undefined
      }
      this.handleFile = this.handleFile.bind(this)
      this.handleChange = this.handleChange.bind(this)
    }
   
    handleChange(e) {
      const files = e.target.files
      if (files && files[0]) this.setState({ file: files[0] })
    }
   
    handleFile() {
      /* Boilerplate to set up FileReader */
      const reader = new FileReader()
      const rABS = !!reader.readAsBinaryString
   
      reader.onload = (e) => {
        /* Parse data */
        const bstr = e.target.result
        const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA : true })

        /* Get first worksheet */
        const wsname = wb.SheetNames[0]
        const ws = wb.Sheets[wsname]

        /* Convert array of arrays */
        const data = XLSX.utils.sheet_to_json(ws, {range:1})
       

        /* Update state */
        this.setState({ data: data, cols: make_cols(ws['!ref']), display: this.state.data }, () => {
            // console.log(JSON.stringify(this.state.data, null, 2))
            // console.log(this.state.data)
        })
      }
   
      if (rABS) {
        reader.readAsBinaryString(this.state.file)
      } else {
        reader.readAsArrayBuffer(this.state.file)
      }
    }
    render() {
        return (
            <div>
                <h3>Bulk Uploads Demo</h3>
                <input type="file" className="form-control" id="file" accept={SheetJSFT} onChange={this.handleChange} />
                <br />
                <input type='submit' 
                    value="Load Data"
                    onClick={this.handleFile} 
                />
                <BulkUpload bulkData={this.state.data}/>
          </div>
        )
    }
}

export default ExcelReader

