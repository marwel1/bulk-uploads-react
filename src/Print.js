import React, { useState, useEffect, createRef } from 'react'
import request from 'superagent'

import Pdf from "react-to-pdf"
import Barcode from 'react-barcode'

const Print = () => {
    const ref = createRef()
    const [gmrId, setGmrId] = useState()
    const [state, setstate] = useState()
    const [err, setErr] = useState()

    // Fetch actual data in SandBox directly from the GVMS API
    // Token Expires after 4hrs
    const testRun = () => { 
        const reqNotif = request
            .get(`https://cors-anywhere.herokuapp.com/https://test-api.service.hmrc.gov.uk/customs/goods-movement-system/movements/${gmrId}`)
            .accept('application/vnd.hmrc.1.0+json')
            .set('Authorization', `Bearer 9d611a776b11ff64f9a6788637742756`)
            .then((resp) => setstate(resp.body))
            .catch((err) => setErr(err))
    }

    useEffect(() => {
        // gmrId && testRun()
    }, [gmrId])

    return (
        <div>
            TEST GMRS: GMROTVJ3XQZT - GMROS6DG07XB<br/>
            <input type='text' onChange={(e)=>setGmrId(e.target.value)} />
            <button onClick={testRun}>Search</button>

            {
                state && !err ?
                <div ref={ref} style={{margin: '0.5em', padding: '2em 0 2em 3em'}}>
                    <Barcode value={state.gmrId.toUpperCase()} height='70' width='3' fontSize='30'/>
                    <p style={{color: 'black', marginTop: '0.5em'}}>GMR ID: <br/>{state.gmrId}</p>
                    <p style={{color: 'black'}}>Direction: <br/>{state.direction}</p>
                    <p style={{color: 'black'}}>Haulier EORI: <br/>{state.haulierEORI}</p>
                    <p style={{color: 'black'}}>State: <br/>{state.metadata.state}</p>
                    <p style={{color: 'black'}}>Unaccompanied: <br/>{state.isUnaccompanied ? state.isUnaccompanied.toString() : 'Not defined'}</p>
                    <p style={{color: 'black'}}>trailer Registration Numbers: 
                        <br/>{state.trailerRegistrationNums 
                        ? state.trailerRegistrationNums.map( trailer => <p style={{color: 'black', marginTop: '0.5em'}}>{trailer}</p>) 
                        : 'Not defined'}
                    </p>
                    <p style={{color: 'black'}}>Planned Crossing: 
                        <br/>{state.plannedCrossing 
                        ? <div>
                            <p style={{color: 'black', marginTop: '0.5em'}}>- Route ID:{state.plannedCrossing.routeId}</p>
                            <p style={{color: 'black'}}>- Local Departure D & T{state.plannedCrossing.localDateTimeOfDeparture}</p>
                         </div>
                        : 'Not defined'}
                    </p>
                    <p style={{color: 'black'}}>MRN: <br/>{state.sAndSMasterRefNum ? state.sAndSMasterRefNum : 'Not defined'}</p>
                    <p style={{color: 'black'}}>TIR Declarations: 
                        <br/>{state.tirDeclarations 
                        ? state.tirDeclarations.map( tir => <p style={{color: 'black', marginTop: '0.5em'}}>- TIR Carnet Id: {tir.tirCarnetId}</p>) 
                        : 'Not defined'}
                    </p>
                    <p style={{color: 'black', marginTop: '2em'}}>
                        Rule Failures: 
                        { 
                            state.metadata.ruleFailures ?
                            state.metadata.ruleFailures.map( rf => <p style={{color: 'black', marginTop: '0.5em'}}> • {rf.technicalMessage}</p>)
                            : 'none'
                        }
                    </p>
                </div>
                : <p style={{color: 'black', marginTop: '1.5em'}}>Token Expired</p>
            }

            <Pdf targetRef={ref} filename='Sample.pdf'>
                {({ toPdf }) => <button onClick={toPdf}>Download as PDF</button>}
            </Pdf>
        </div>
    )
}

export default Print
